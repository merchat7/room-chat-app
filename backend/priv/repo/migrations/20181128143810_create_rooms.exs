defmodule Backend.Repo.Migrations.CreateRooms do
  use Ecto.Migration

  def change do
    create table(:rooms, primary_key: false) do
      add :name, :string, primary_key: true
      add :owner, :string

      timestamps()
    end

  end
end
