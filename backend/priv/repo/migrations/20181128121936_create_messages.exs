defmodule Backend.Repo.Migrations.CreateMessages do
  use Ecto.Migration

  def change do
    create table(:messages) do
      add :username, :string
      add :message, :string
      add :room, :string

      timestamps()
    end

  end
end
