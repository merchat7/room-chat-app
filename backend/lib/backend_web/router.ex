defmodule BackendWeb.Router do
  use BackendWeb, :router

  pipeline :api do
    plug CORSPlug, origin: "*"
    plug :accepts, ["json"]
  end

  scope "/api", BackendWeb do
    pipe_through :api

    resources "/rooms", RoomController, except: [:new, :edit, :delete]
    delete "/rooms/:id/:owner", RoomController, :delete
    options "/rooms", RoomController, :create
  end
end
