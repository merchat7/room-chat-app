defmodule BackendWeb.RoomView do
  use BackendWeb, :view
  alias BackendWeb.RoomView

  def render("index.json", %{rooms: rooms}) do
    %{data: render_many(rooms, RoomView, "room.json")}
  end

  def render("show.json", %{room: room}) do
    %{data: render_one(room, RoomView, "room.json")}
  end

  def render("room.json", %{room: room}) do
    %{name: room.name,
      owner: room.owner}
  end
end
