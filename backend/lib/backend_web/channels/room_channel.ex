defmodule BackendWeb.RoomChannel do
  use BackendWeb, :channel

  def join("rooms:" <> room, payload, socket) do
    if authorized?(payload) do
      if !Map.has_key?(BackendWeb.Presence.list(socket), socket.assigns.username) do
        send(self(), {:after_join, room})
        {:ok, room, assign(socket, :room, room)}
      else
        {:error, %{reason: "already in room"}}
      end
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (room:lobby).
  def handle_in("shout", payload, socket) do
    Backend.Message.changeset(%Backend.Message{}, Map.put(payload, "room", socket.assigns.room)) |> Backend.Repo.insert
    broadcast socket, "shout", payload
    {:noreply, socket}
  end

  def handle_in("typing", payload, socket) do
    BackendWeb.Presence.update(socket, socket.assigns.username, %{
      user: socket.assigns.username,
      typing: payload
    })
    {:noreply, socket}
  end


  def handle_info({:after_join, room}, socket) do
    # Must send current presence_state before tracking or else race condition with event "presence_diff"
    Backend.Message.get_messages(room, 100)
    |> Enum.reverse
    |> Enum.each(fn msg -> push(socket, "shout", %{
      username: msg.username,
      message: msg.message,
    }) end)
    push(socket, "presence_state", BackendWeb.Presence.list(socket))
    BackendWeb.Presence.track(socket, socket.assigns.username, %{
      user: socket.assigns.username,
      typing: false
    })
    {:noreply, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end
