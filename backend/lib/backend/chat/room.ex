defmodule Backend.Chat.Room do
  use Ecto.Schema
  import Ecto.Changeset


  @primary_key {:name, :string, []}
  @derive {Phoenix.Param, key: :name}
  schema "rooms" do
    field :owner, :string
    timestamps()
  end

  @doc false
  def changeset(room, attrs) do
    room
    |> cast(attrs, [:name, :owner])
    |> validate_required([:name, :owner])
    |> unique_constraint(:name, name: :rooms_name_index)
  end
end
