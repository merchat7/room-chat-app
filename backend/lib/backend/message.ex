defmodule Backend.Message do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query


  schema "messages" do
    field :message, :string
    field :room, :string
    field :username, :string

    timestamps()
  end

  def get_messages(room, limit) do
    Backend.Repo.all(
      from msg in Backend.Message,
        where: msg.room == ^room,
        order_by: [desc: msg.inserted_at],
        limit: ^limit,
        select: %{message: msg.message, username: msg.username}
    )
  end

  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, [:username, :message, :room])
    |> validate_required([:username, :message, :room])
  end
end
