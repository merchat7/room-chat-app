import React, { Component } from 'react';
import { Socket, Presence } from 'phoenix';
import Messages from './Messages';
import ChatInput from './ChatInput';
import '../styles/ChatApp.css';
import 'status-indicator/styles.css'

class ChatApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      presence: {},
      onlineUsers: [],
      typingUsers: "",
      disconnected: false,
    };
    this.sendHandler = this.sendHandler.bind(this);
  }

  componentDidMount() {
    // Connect to the server
    this.socket = new Socket("ws://localhost:4000/socket", {params : {username: this.props.username}});  // url should be moved to config file
    this.socket.connect();
    this.socket.onError(() => {
      if (!this.state.disconnected) {
        const messages = this.state.messages;
        messages.push({username: "System", message: "Disconnected, reconnecting..."});
        this.setState({messages, disconnected: true});
      }
    });
    // Connect to a "room"
    this.channel = this.socket.channel('rooms:' + this.props.room);
    this.channel.join()
      .receive("ok", response => {
        console.log(`Joined room '${response}', successful`);
        this.setState({messages: [], disconnected: false});
      }) // Should get existing message in room here in the future
      .receive("error", ({reason}) => {
        if (reason === "already in room") {
          alert(reason);
          this.props.goBack();
        }
      })
      .receive("timeout", () => console.log("Networking issue. Still waiting..."));

    // Listen to message from self & other users
    this.channel.on('shout', message => {
      if (message.username === this.props.username) message.fromMe = true;
      this.addMessage(message);
    });

    this.channel.on('presence_state', currentOnlineUser => { // Should only execute once per connection
      let newPresence = Presence.syncState({}, currentOnlineUser); // {} to prevent any weird error on disconnect -> reconnect
      this.setState({presence: newPresence});
      this.processPresence(newPresence);
    });

    this.channel.on('presence_diff', changeOnlineUser => { // The change to be updated in presence_state
      let newPresence = Presence.syncDiff(this.state.presence, changeOnlineUser);
      this.setState({presence: newPresence});
      this.processPresence(newPresence);
    });
  }

  componentWillUnmount() {
    this.channel.leave() // Will also unsubscribe from all events
      .receive("ok", () => { console.log(`Left room '${this.props.room}', successful`)})
  }

  processPresence(presence) {
    let newOnlineUsers = [];
    let newTypingUsers = "";
    let typingUserCount = 0;
    Object.keys(presence).forEach(key => {
      let onlineUser = presence[key]["metas"][0]["user"];
      let isTyping = presence[key]["metas"][0]["typing"];
      newOnlineUsers.push(onlineUser);
      if (isTyping) {
        typingUserCount += 1;
        newTypingUsers += `${onlineUser}, `;
      }
    });
    newTypingUsers = newTypingUsers.substr(0, newTypingUsers.length-2); // Remove commas
    if (newTypingUsers.length > 150) {
      if (typingUserCount === 1) newTypingUsers = "A user is typing...";
      else  newTypingUsers = "Multiple users are typing...";
    }
    else if (typingUserCount === 1) newTypingUsers += " is typing";
    else if (typingUserCount > 1) newTypingUsers += " are typing...";
    this.setState({
      onlineUsers: newOnlineUsers,
      typingUsers: newTypingUsers
    });
  }

  sendHandler(message) {
    const messageObject = {
      username: this.props.username,
      message,
    };
    // Broadcast the message to other users in room (including self)
    this.channel.push("shout", messageObject);
  }

  addMessage(message) {
    // Append the message to the component state
    const messages = this.state.messages;
    messages.push(message);
    this.setState({ messages });
  }

  render() {
    const {onlineUsers, typingUsers, disconnected}  = this.state;
    const onlineUsersElements = onlineUsers.map(onlineUser => {
      const isMe = this.props.username === onlineUser ? "me" : "";
      if (isMe) onlineUser += " (me)";
      return (
        <div key={onlineUser} className={`user-container ${isMe} unselectable`}>
          <status-indicator positive pulse/>
          {` ${onlineUser}`}
        </div>
      );
    });

    return (
      <div className="container">
        <div className={`side-container users-container`}>
          <div className="heading-container unselectable">
            Menu
          </div>
          <button className="menu-button" onClick={() => this.props.goBack(this.props.username)}>
            Room Select
          </button>
          <button className="menu-button" onClick={() => this.props.goBack(this.props.username)}>
            Activate Lasers
          </button>
          <div className={"heading-container unselectable"}>
            Users
          </div>
          {onlineUsersElements}
        </div>
        <div className={`side-container chat-container`}>
          <h3 className="unselectable">{this.props.room}</h3>
          <Messages messages={this.state.messages} />
          <div className="is-typing-container">
            {typingUsers}
          </div>
          <ChatInput onSend={this.sendHandler} channel={this.channel} disconnected={disconnected} />
        </div>
      </div>
    );
  }

}
ChatApp.defaultProps = {
  username: 'Anonymous'
};

export default ChatApp;
