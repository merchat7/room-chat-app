import React, { Component } from 'react';

class ChatInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chatInput: '',
      disconnected: this.props.disconnected
    };

    // React ES6 does not bind 'this' to event handlers by default
    this.submitHandler = this.submitHandler.bind(this);
    this.textChangeHandler = this.textChangeHandler.bind(this);
  }

  submitHandler(event) {
    // Stop the form from refreshing the page on submit
    event.preventDefault();

    // Clear the input box
    this.setState({ chatInput: '' });

    // Call the onSend callback with the chatInput message
    this.props.onSend(this.state.chatInput);
    this.props.channel.push("typing", false);
  }

  textChangeHandler(event)  {
    this.setState({ chatInput: event.target.value });
    if (event.target.value.length > 0) this.props.channel.push("typing", true);
    else this.props.channel.push("typing", false);
  }

  componentDidUpdate(prevProps) {
    if(this.props.disconnected !== prevProps.disconnected) {
      this.setState({disconnected: this.props.disconnected})
    }
  }


  render() {
    return (
      <form className="chat-input" onSubmit={this.submitHandler}>
        <input type="text"
               className="input-field"
               onChange={this.textChangeHandler}
               value={this.state.chatInput}
               placeholder="Write a message..."
               required
               disabled={this.state.disconnected}/>
      </form>
    );
  }
}

ChatInput.defaultProps = {
};

export default ChatInput;
