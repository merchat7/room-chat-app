import React, {Component} from 'react';
import './styles/App.css';
import './styles/Login.css';
import ChatApp from "./components/ChatApp";
import axios from "axios"
import CreatableSelect from 'react-select/lib/Creatable';

const appNames = ['Zlack', "Accord"];

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      room: '',
      isLoading: true,
      createRoom: false,
      rooms: []
    };
    this.appName = appNames[Math.floor(Math.random()*appNames.length)] + "™";

    // Bind 'this' to event handlers. React ES6 does not do this by default
    this.usernameChangeHandler = this.usernameChangeHandler.bind(this);
    this.formSubmitHandler = this.formSubmitHandler.bind(this);
    this.goBack = this.goBack.bind(this)
  }

  componentDidMount() {
    this.getRoomList()
  }

  getRoomList() {
    this.setState({isLoading: true});
    axios.get('http://localhost:4000/api/rooms')
      .then(response => {
        let rooms = [];
        response.data["data"].forEach(room => {
          rooms.push({
            value: room.name,
            label: room.name,
            owner: room.owner,
            id: room.id
          })
        });
        this.setState({rooms: rooms, isLoading: false})
      })
  }

  createNewRoom() {
    axios({
      method: 'post',
      headers: {
        "Content-Type": "application/json",
      },
      url: 'http://localhost:4000/api/rooms',
      data: {
        "room": {
          "name": this.state.room,
          "owner": this.state.username
        }
      }
    }).then(() => this.setState({ submitted: true, createRoom: false}))
      .catch(() => {
        alert("An error occured")
      })
  }

  usernameChangeHandler(event) {
    this.setState({ username: event.target.value });
  }

  formSubmitHandler(event) {
    event.preventDefault();
    if (this.state.room) {
      if (this.state.createRoom) this.createNewRoom();
      else this.setState({ submitted: true});
    }

    else (alert("Must select a room"))
  }

  goBack = (username) => {
    this.setState({
      submitted: false,
      username: username,
      room: ""
    });
    this.getRoomList()
  };

  handleChange = (newValue: any, actionMeta: any) => {
    const action = actionMeta.action.toString();
    if (action === "create-option") {
      this.setState({room: newValue.value, createRoom: true});
    }
    else if (action === "select-option") {
      this.setState({room: newValue.value, createRoom: false});
    }

  };


  render() {
    if (this.state.isLoading) {
      return (
        <img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif" alt="loading" className="image"/>
      )
    }
    else if (this.state.submitted) {
      // Form was submitted, now show the main App
      return (
        <ChatApp username={this.state.username} room={this.state.room} goBack = {this.goBack}/>
      );
    }

    // Initial page load, show a simple login form
    return (
      <form onSubmit={this.formSubmitHandler} className="username-container">
        <h1>{this.appName}</h1>
        <div>
          <input
            className ="input-field"
            type="text"
            value={this.state.username}
            onChange={this.usernameChangeHandler}
            placeholder="Enter a username..."
            required
            pattern="[A-Za-z0-9]{1,}"
            title="Letters & numbers only"/>
          <CreatableSelect
            isClearable
            placeholder={"Select a room..."}
            className={"room-select-container"}
            classNamePrefix={"room-select"}
            onChange={this.handleChange}
            options={this.state.rooms}
          />
        </div>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}

App.defaultProps = {
};

export default App;
